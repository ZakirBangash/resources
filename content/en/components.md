---
title: Components
description: ""
position: 2
category: Guide
---

### Author

<author name="Arsala" desc="President at Grey Software" linkedin-url="https://linkedin.com/in/ArsalaBangash" twitter="arsalagrey" avatar-url="https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png" gitlab-url="https://gitlab.com/ArsalaBangash" github-url="https://github.com/ArsalaBangash" ></author>

### CTA Button

<cta-button text="Explore" link="https://ecosystem.grey.software">
</cta-button>

<br></br>
